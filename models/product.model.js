/**
 * Category Sub Schema
 */
// let CategorySubSchema = new mongoose.Schema({
//     ObjectId: {
//         type: mongoose.Schema.Types.ObjectId,
//         ref: 'Category'
//     },
//     Status: {
//         type: String
//     }
// });

/**
 * Product Schema
 */
let ProductSchema = new mongoose.Schema({
    Name: {
        type: String
    },
    Quantity: {
        type: Number
    },
    Price: {
        type: Number
    },
    Status: {
        type: String
    },
    // Category: CategorySubSchema,
    CategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    }
}, {timestamps: true});

mongoose.model('Product', ProductSchema);
global.Product = mongoose.models.Product;

let fs = require('fs');
class Apis {
    constructor(app) {
        app.use('/api', (req, res, next) => {
            next();
        });
        this.setupRoutes(app);
    }

    setupRoutes(app) {
        fs.readdirSync(__dirname + '/').filter((file) => {
            const stats = fs.statSync(__dirname + '/' + file);
            return (file.indexOf('.') !== 0 && stats.isDirectory());
        }).forEach((file) => {
            let initRoute = require(__dirname + '/' + file);
            new initRoute(app);
        });
    }
}

module.exports = Apis;

/*
Product : name, quantity, price, category : objectId, status
Category : name, status


=> Product list with paging with category name
=> Product list filter for price filter like, less, greater, between
=> Product list filter based on name, category search
=> Get category detail with its product
 */
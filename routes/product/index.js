class ProductController {
    constructor(app) {
        app.get('/api/products', this.listProduct);
        app.get('/api/filter-products', this.filterProduct);
        app.get('/api/search-products/:productName/:categoryName', this.searchProduct);

        app.get('/api/products/:id', this.getProduct);
        app.post('/api/products', this.createProduct);
        app.put('/api/products/:id', this.updateProduct);
        app.delete('/api/products/:id', this.removeProduct);


    }


    validateUser() {

    }

    listProduct(req, res) {
        // => Product list with paging with category name
        let pageNo = 1,
            rows = 50,
            sortOrder = 'DESC',
            sortBy = null,
            whereCondition = {};

        console.log('req.quary', req.query);

        req.query.page ? pageNo = parseInt(req.query.page) : '';
        req.query.limit ? rows = parseInt(req.query.limit) : '';
        console.log('pageNo', pageNo);
        console.log('rows', rows);

        if (pageNo > 0) {
            global.Product.find()
                .populate('CategoryId','Name -_id' /*{/*Name:1 */ /*_id: 0, Status: 0, createdAt: 0, updatedAt: 0, __v: 0}*/) //0 means hide, 1 means show
                .limit(rows)
                .skip((pageNo - 1) * rows)
                .then((product) => {
                    console.log('product', product);

                    res.send(product);
                }).catch((error) => {
                console.log('error', error);
                res.send(error);
            });
        } else {
            res.send(400, "Page number must be positive integer.");
        }
        /*  global.Product.find()
              .limit(rows)
              .skip((pageNo - 1) * rows)
              .then((product) => {
                  res.send(product);
              }).catch((error) => {
              console.log('error', error);
              res.send(error);
          })*/

    }

    filterProduct(req, res) {
        // => Product list filter for price filter like, less, greater, between
        let graterPrice = 0, lessarPrice = 1000;
        console.log('req.quary', req.query);
        req.query.grater ? graterPrice = req.query.grater : '';
        req.query.lessar ? lessarPrice = req.query.lessar : '';

        console.log('graterPrice', graterPrice);
        console.log('lessarPrice', lessarPrice);

        global.Product.find({
            Price: {
                $lt: lessarPrice,
                $gt: graterPrice
            }
        })
            .then((product) => {
                console.log('product', product);

                res.send(product);
            }).catch((error) => {
            console.log('error', error);
            res.send(error);
        })
    }


    searchProduct(req, res) {
        //=> Product list filter based on name, category search
        console.log('Called');
        let productSearch = req.params.productName,
            categorySearch = req.params.categoryName;

        /*global.Product.find()
            .then((product) => {
                res.send(product);
            }).catch((error) => {
            console.log('error', error);
            res.send(error);
        })*/

        /*global.Product.aggregate([
            {
                "$lookup":
                    {
                        from: "Category",
                        localField: "CategoryId",
                        foreignField: "_id",
                        as: "Category_Data"
                    }
            }
        ])*/
        global.Product.aggregate([
            {
                $lookup:
                    {
                        from: "categories",
                        localField: "CategoryId",
                        foreignField: "_id",
                        as: "inventory_docs"
                    }
            }
        ])
            .then((response) => {
            console.log('response', response);

            res.send(response);
        }).catch((error) => {
            res.send(error);
        });
    }


    getProduct(req, res) {
        let productId = req.params.id;
        global.Product.findOne({_id: productId}).then((product) => {
            res.send(product);
        }).catch((error) => {
            console.log('error', error);
            res.send(error);
        })
    }

    createProduct(req, res) {
        let name = req.body.name,
            quantity = req.body.quantity,
            price = req.body.price,
            status = req.body.status,
            categoryId = req.body.categoryId;

        let product = new global.Product;
        product.Name = name;
        product.Quantity = quantity;
        product.Price = price;
        product.Status = status;
        product.CategoryId = categoryId;
        product.save().then((response) => {
            console.log('response', response);
            res.send(response);
        }).catch((error) => {
            console.log('error', error);
            res.send(error);
        });
    }

    updateProduct(req, res) {
        let userId = req.params.id;

    }

    removeProduct(req, res) {
        let userId = req.params.id;
    }


}

module.exports = ProductController;


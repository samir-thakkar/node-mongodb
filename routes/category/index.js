class CategoryController {
    constructor(app) {
        app.get('/api/category', this.listCategory);
        app.get('/api/category/:id', this.temp);
        app.post('/api/category', this.createCategory);
        app.put('/api/category/:id', this.updateCategory);
        app.delete('/api/category/:id', this.temp);
    }

    temp() {

    }

    createCategory(req, res) {
        let categoryName = req.body.category,
            categoryStatus = req.body.status;

        let category = new global.Category;
        category.Name = categoryName;
        category.Status = categoryStatus;
        category.save().then((response) => {
            res.send(response);
        }).catch((error) => {
            res.send(error);
        });
    }

    listCategory(req, res) {
        //=> Get category detail with its product

        global.Category.aggregate([
            {
                $lookup:
                    {
                        from: "products", //It is not model name but it is a name of collection.
                        localField: "_id",
                        foreignField: "CategoryId",
                        as: "product_detail"
                    }
            }
        ]).then((response) => {
            console.log('response', response);

            res.send(response);
        }).catch((error) => {
            res.send(error);
        });
    }

    updateCategory(req, res) {
        let categoryId = req.params.id;
    }

}

module.exports = CategoryController;